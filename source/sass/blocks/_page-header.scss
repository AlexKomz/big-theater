.page-header {
  color: $color;

  background-image: url("../img/big-theatre-main-bg.jpg");
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;

  margin-bottom: 80px;

  @media (min-resolution: $retina-dpi),
    (min-resolution: $retina-dppx) {
    background-image: url("../img/big-theatre-main-bg@2x.jpg");
  }
}

.page-header__overview {
  position: relative;

  display: grid;
  grid-template-rows: min-content 1fr;

  height: 100vh;

  background-color: rgba($color: $bg-color, $alpha: 0.85);
}

.page-header__contacts {
  display: none;

  @media (min-width: $large-width) {
    position: absolute;

    top: 0;
    left: 0;

    display: flex;
  }
}

.page-header__wrapper {
  display: flex;
  justify-content: flex-end;

  @media (min-width: $large-width) {
    display: grid;
    justify-content: stretch;
    gap: 50px;

    margin-top: 51px;
  }

  @media (min-width: $full-width) {
    display: flex;
    justify-content: space-between;

    @include container(1110px);
  }
}

.page-header__phone {
  margin: 56px 39px 0 0;

  position: relative;

  font-family: "Open Sans Condensed", sans-serif;

  font-weight: 700;
  font-size: 16px;
  line-height: 22px;
  letter-spacing: 0.05em;

  text-transform: uppercase;

  &::after {
    content: "";

    display: block;

    position: absolute;

    width: 100%;
    height: 2px;

    background-color: $main-color;
  }

  @media (min-width: $large-width) {
    margin: 0;

    justify-self: center;
  }
}

.page-header__preview {
  align-self: center;

  margin: 0 auto;
  margin-top: -78px;

  @media (min-width: $large-width) {
    margin-left: 191px;
    margin-top: -148px;
  }

  @media (min-width: $full-width) {
    @include container(1110px);

    margin-top: -76px;
  }
}
