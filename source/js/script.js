const tape = document.querySelector(`.tape__list`);
const tapeItems = document.querySelectorAll(`.tape__item`);

tapeItems.forEach((item) => {
  item.addEventListener(`mousedown`, (evt) => {
    evt.preventDefault();

    const point = evt.clientX;

    const tapePosition = tape.offsetLeft;

    const target = evt.currentTarget;

    const overview = document.createElement(`div`);
    overview.style.height = `100%`;
    overview.style.width = `100%`;
    overview.style.position = `absolute`;
    overview.style.left = '0px';
    overview.style.top = '0px';

    const onMouseMove = (evt) => {
      target.append(overview);

      tape.style.cursor = `grab`;
      tape.style.marginLeft = `${tapePosition + evt.clientX - point}px`;
    };

    const onMouseUp = () => {
      overview.remove();

      tape.style.cursor = `pointer`;

      document.removeEventListener(`mousemove`, onMouseMove);
      document.removeEventListener(`mouseup`, onMouseUp);
    };

    document.addEventListener(`mousemove`, onMouseMove);
    document.addEventListener(`mouseup`, onMouseUp);
  });
});

// tape.addEventListener(`mousedown`, (evt) => {
//   evt.preventDefault();

//   const point = evt.clientX;

//   const tapePosition = tape.offsetLeft;

//   const onMouseMove = (evt) => {
//     tape.style.cursor = `grab`;
//     tape.style.marginLeft = `${tapePosition + evt.clientX - point}px`;
//   };

//   const onMouseUp = () => {
//     tape.style.cursor = `pointer`;

//     document.removeEventListener(`mousemove`, onMouseMove);
//     document.removeEventListener(`mouseup`, onMouseUp);
//   };

//   document.addEventListener(`mousemove`, onMouseMove);
//   document.addEventListener(`mouseup`, onMouseUp);
// });
